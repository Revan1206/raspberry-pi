$(document).ready(function () {
    waitForDocumentLoaded();
});

function waitForDocumentLoaded() {
    // sonarqube hat die seite fertig dargestellt
    if ($('.spacer-top').length === 0) {
        setTimeout(waitForDocumentLoaded, 1000);
        return;
    }
    modifyPage();
}

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) {
        return;
    }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}
