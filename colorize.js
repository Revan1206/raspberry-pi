// ==UserScript==
// @name         Grüne Farbe
// @namespace    http://tampermonkey.net/
// @version      0.8
// @description  try to take over the world!
// @author       You
// @match        https://sonarqube.mk-dev-test.com/projects?sort=-coverage
// @grant        unsafeWindow
// @run-at       document-end
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @updateURL    https://gitlab.com/Revan1206/raspberry-pi/-/raw/master/colorize.js
// @downloadURL  https://gitlab.com/Revan1206/raspberry-pi/-/raw/master/colorize.js
// ==/UserScript==


const $ = window.$;

$(document).ready(function () {
    waitForDocumentLoaded();
});

function waitForDocumentLoaded() {
    // sonarqube hat die seite fertig dargestellt
    if ($('.spacer-top').length === 0) {
        setTimeout(waitForDocumentLoaded, 1000);
        return;
    }
    modifyPage();
}

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) {
        return;
    }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}


addGlobalStyle('.own-style-ok { background-color:green !important; }');
addGlobalStyle('.own-style-bad { background-color:red !important; }');

function modifyPage() {
    $(".boxed-group").each(function () {
        const card = $(this).find('.project-card-measure')[3];

        // span rausfinden der die %-zahl beinhaltet
        const percentDiv = $(card).find('span')[1];

        const percent = parseInt($(percentDiv).html());

        if (percent >= 70) {
            $(this).addClass("own-style-ok");
        }
    });
}
