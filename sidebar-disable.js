// ==UserScript==
// @name         Sidebar disable
// @namespace    http://tampermonkey.net/
// @version      0.1.7
// @description  try to take over the world!
// @author       You
// @grant        unsafeWindow
// @match        https://sonarqube.mk-dev-test.com/projects?sort=-coverage
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @updateURL    https://gitlab.com/Revan1206/raspberry-pi/-/raw/master/sidebar-disable.js
// @downloadURL  https://gitlab.com/Revan1206/raspberry-pi/-/raw/master/sidebar-disable.js
// ==/UserScript==

$(document).ready(function () {
    waitForDocumentLoaded();
});

function waitForDocumentLoaded() {
    // sonarqube hat die seite fertig dargestellt
    if ($('.spacer-top').length === 0) {
        setTimeout(waitForDocumentLoaded, 1000);
        return;
    }
    modifyPage();
}

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) {
        return;
    }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

var $ = window.$;

function modifyPage() {
    $(document).ready(function () {
        $(".layout-page-side-outer").first().hide();
    })();
}
